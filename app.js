const express = require('express');
const bodyParser = require('body-parser');
const json = require('body-parser/lib/types/json');
const _ = require('lodash');

const app = express();
const port = 3000;

const homeStartingContent = "This is a web app where it will enable user to create journal entries.";
const aboutContent = "This an about.";
const contactContent = "This is where you can contact us.";



const posts = [];
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine','ejs');


app.listen(port,function(){
    console.log("Sever is running on port "+port+"!");
});

app.get('/', (req, res) => {

    res.render('home.ejs', {
        startingContent : homeStartingContent,
        post: posts    
    });
});

app.get('/about', function(req, res){
    res.render('about.ejs',{about: aboutContent});
});

app.get('/contacts', function(req, res){
    res.render('contact.ejs', {contacts: contactContent});
});


app.get('/compose', function(req, res){
    res.render('compose.ejs');
});

app.post('/compose', function(req, res){
    let title = req.body.title;
    let content = req.body.content;
    let post = {
        Title : title,
        Content : content
    }
    posts.push(post);
    res.redirect("/");
});

app.get('/posts/:route',function(req, res){
    let rParam = _.lowerCase(req.params.route);

    posts.forEach(post => {
        const storedPosts = _.lowerCase(post.Title);

        if(storedPosts===rParam){
            res.render('post.ejs', {postTitle : post.Title, postContent: post.Content});
        }
    });
});